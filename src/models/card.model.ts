export class CardModel {
  private readonly _stockName: string;
  private readonly _stockSymbol: string;
  private readonly _stockPrice: number;
  private readonly _stockTodayChangePrice: number;
  private readonly _stockTodayChangePercent: string;
  private readonly _stockTodayOpen: number;
  private readonly _stockTodayHigh: number;
  private readonly _stockTodayLow: number;

  constructor(
    stockName: string,
    stockSymbol: string,
    stockPrice: number,
    stockTodayChangePrice: number,
    stockTodayChangePercent: string,
    stockTodayOpen: number,
    stockTodayHigh: number,
    stockTodayLow: number
  ) {
    this._stockName = stockName;
    this._stockSymbol = stockSymbol;
    this._stockPrice = stockPrice;
    this._stockTodayChangePrice = stockTodayChangePrice;
    this._stockTodayChangePercent = stockTodayChangePercent;
    this._stockTodayOpen = stockTodayOpen;
    this._stockTodayHigh = stockTodayHigh;
    this._stockTodayLow = stockTodayLow;
  }

  get stockName(): string {
    return this._stockName;
  }

  get stockSymbol(): string {
    return this._stockSymbol;
  }

  get stockPrice(): number {
    return this._stockPrice;
  }

  get stockTodayChangePrice(): number {
    return this._stockTodayChangePrice;
  }

  get stockTodayChangePercent(): string {
    return this._stockTodayChangePercent;
  }

  get stockTodayOpen(): number {
    return this._stockTodayOpen;
  }

  get stockTodayHigh(): number {
    return this._stockTodayHigh;
  }

  get stockTodayLow(): number {
    return this._stockTodayLow;
  }
}

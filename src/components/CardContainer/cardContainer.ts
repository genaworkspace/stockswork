import { CardModel } from "@/models/card.model.ts";
import Card from '@/components/Card/card.vue';
import { Component, Prop, Vue } from "vue-property-decorator";

@Component({
  components: {
    Card
  }
})
export default class CardContainer extends Vue {
  @Prop({ required: true }) cardModels: CardModel[];
  constructor() {
    super();
  }
}

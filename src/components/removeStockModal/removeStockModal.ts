import {Component, Prop, Vue, Watch} from "vue-property-decorator";

@Component({})
export default class RemoveStockModal extends Vue {

    dialog:boolean;
    stockSymbol:string;
    constructor() {
        super();
        this.dialog=true;
        this.stockSymbol="";
    }

    onCloseClicked(){
        this.$emit("modalCancelClicked");
    }

    onRemoveClicked(){
        this.$emit("modalRemoveClicked", this.stockSymbol);
    }
}

import { CardModel } from "@/models/card.model.ts";
import { Component, Prop, Vue } from "vue-property-decorator";

@Component({})
export default class Card extends Vue {
  @Prop({ required: true }) cardModel: CardModel;

  raised:boolean;
  constructor() {
    super();
    this.raised=true;
  }
}

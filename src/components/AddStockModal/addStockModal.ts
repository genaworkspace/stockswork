import {Component, Prop, Vue, Watch} from "vue-property-decorator";

@Component({})
export default class AddStockModal extends Vue {

    dialog:boolean;
    stockSymbol:string;
    constructor() {
        super();
        this.dialog=true;
        this.stockSymbol="";
    }

    onCloseClicked(){
        this.$emit("modalCancelClicked");
    }

    onSaveClicked(){
        this.$emit("modalSaveClicked", this.stockSymbol);
    }
}

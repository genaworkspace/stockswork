import Vue from "vue";
import Component from "vue-class-component";
import { CardModel } from "@/models/card.model.ts";
import CardContainer from '@/components/CardContainer/cardContainer.vue';
import AddStockModal from '@/components/AddStockModal/addStockModal.vue';
import RemoveStockModal from '@/components/removeStockModal/removeStockModal.vue';

// The @Component decorator indicates the class is a Vue component
@Component({
  components: {
    CardContainer,
    AddStockModal,
    RemoveStockModal
  }
})
export default class Gena extends Vue {
  cardModels: CardModel[];
  drawer:boolean;
  showAddNewStockModal: boolean;
  showRemoveStockModal: boolean;
  constructor() {
    super();
    this.drawer=false;
    this.showAddNewStockModal=false;
    this.showRemoveStockModal=false;
    this.cardModels=[];
    for (let i=0; i<30; i++)
    {
      this.cardModels.push(new CardModel(
          "Intel Corporation",
          "INTC",
          58.75,
          -0.72,
          "4",
          59.99,
          60.22,
          58.26
      ));
    }
  }


  //buttons clicked
  addNewStockClicked(){
    this.showAddNewStockModal=true;

  }

  removeStockClicked(){
     this.showRemoveStockModal=true;
  }

  //==============Events==============

  //events from add stock modal
  modalAddStockCancelClicked(){
    this.showAddNewStockModal=false;
  }

  modalAddStockSaveClicked(symbol: string) :void{
    this.showAddNewStockModal=false;
    this.cardModels.push(new CardModel(
        "Intel Corporation",
        symbol,
        58.75,
        -0.72,
        "4",
        59.99,
        60.22,
        58.26
    ));
  }

  //events from remove stock

  modalRemoveStockCancelClicked(): void{
    this.showRemoveStockModal=false;
  }

  modalRemoveStockRemoveClicked(stockSymbol: string){
    this.showRemoveStockModal=false;
    this.cardModels= this.cardModels.filter(function( obj ) {
      return obj.stockSymbol !== stockSymbol;
    });
  }
}
